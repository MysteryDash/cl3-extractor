# /!\ Warning /!\

This is an early release of this project.
Please do not expect high performances (although the software is already parallelized).

# How to use

Simply drag and drop the CL3 files you want to convert.
If the input file name is MyFile.cl3, the output folder name will be MyFile.
The folder will be saved in the same folder as the input's one.

# Version

This project's current version is 0.1.0-alpha.

# License

This project is licensed under the terms of the Simple Non Code License 2.0.2.
You can find more details about this license in the License.txt file.