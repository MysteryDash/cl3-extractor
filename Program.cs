﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL3Extractor
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Please drag and drop one or multiple files over this executable.");
            }
            else
            {
                Console.WriteLine("CL3 Extractor - By MysteryDash");

                Parallel.ForEach(args, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, arg =>
                {
                    try
                    {
                        Console.WriteLine($"Processing {arg}...");

                        using (var file = File.Open(arg, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            var reader = new BinaryReader(file);

                            if (new string(reader.ReadChars(3)) != "CL3")
                            {
                                throw new IOException("File isn't a valid CL3 file.");
                            }
                            if (reader.ReadChar() == 'B')
                            {
                                reader = new BigEndianBinaryReader(file);
                            }

                            var directory = Path.ChangeExtension(arg, null);
                            Directory.CreateDirectory(directory);

                            file.Seek(0x60, SeekOrigin.Begin);
                            var fileCount = reader.ReadInt32();

                            file.Seek(0x68, SeekOrigin.Begin);
                            var firstFileOffset = reader.ReadInt32();

                            for (int i = 0; i < fileCount; i++)
                            {
                                file.Seek(i * 0x230 + firstFileOffset, SeekOrigin.Begin);

                                var filename = new string(reader.ReadChars(512)).TrimEnd('\0');
                                file.Seek(0x04, SeekOrigin.Current);
                                var fileOffset = reader.ReadInt32() + firstFileOffset;
                                var fileSize = reader.ReadInt32();

                                file.Seek(fileOffset, SeekOrigin.Begin);
                                File.WriteAllBytes(Path.Combine(directory, filename), reader.ReadBytes(fileSize));
                            }
                            reader.Dispose();
                        }
                    }
                    catch (IOException ex)
                    {
                        Console.WriteLine($"I/O error with {arg}. Details : {ex.Message}");
                    }
                });

                Console.WriteLine("Conversion done !");
                Console.ReadKey();
            }
        }
    }
}